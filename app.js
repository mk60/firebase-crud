const list = document.querySelector('#list-data');
const form = document.querySelector('#add-data');


// db.collection('crud').get().then( snap => {
//     snap.docs.forEach(el => {
//         getData(el);
//     });
// });

db.collection('crud').onSnapshot(snapshot => {
    let changes = snapshot.docChanges();
    console.log(changes)
    changes.forEach(change => {
        if (change.type == 'added') {
            getData(change.doc);
        } else if (change.type == 'removed') {
            let items = document.querySelector(`[data-id=${change.doc.id}]`);
            list.removeChild(items);
        }
    });
});

function getData(doc) {
    const string = `<ul data-id=${doc.id}><li><h3>Name</h3><hr><h3>${doc.data().name}</h3></li>
    <li><h3>College</h3><hr><h3>${doc.data().college}</h3></li>
    <li><h3>Address</h3><hr><h3>${doc.data().address}</h3></li>
    <li><h3>Email</h3><hr><h3>${doc.data().email}</h3></li>
    <li><h3>Phone</h3><hr><h3>${doc.data().phone}</h3></li><button class="update">Update</button><button class="delete">Delete</button><ul>`;

    list.insertAdjacentHTML('afterbegin', string);
    document.querySelector('.delete').addEventListener('click', event => {
        const id = event.target.parentElement.getAttribute('data-id');
        db.collection('crud').doc(id).delete();
    });
    
    document.querySelector('.update').addEventListener('click', event => {
        const id = event.target.parentElement.getAttribute('data-id');
        let data = prompt('Enter data to be changed in JSON format');
        console.log(JSON.stringify(data));
        db.collection('crud').doc(id).update(JSON.parse(data));
    });
}


form.addEventListener('submit', event => {
    event.preventDefault();
    db.collection('crud').add({
        name: form.name.value,
        address: form.address.value,
        email: form.email.value,
        phone: form.phone.value,
        college: form.college.value
    });
    form.name.value = '';
    form.address.value = '';
    form.email.value = '';
    form.phone.value = '';
    form.name.value = '';
    form.college.value = '';
});



